#ifndef OPENRGBRAZEREXTRASPLUGIN_H
#define OPENRGBRAZEREXTRASPLUGIN_H

#include "OpenRGBPluginInterface.h"
#include "ResourceManager.h"
#include "RazerChromaAddressableRGBReset.h"

#include <QObject>
#include <QString>
#include <QtPlugin>
#include <QWidget>

class OpenRGBRazerExtrasPlugin : public QObject, public OpenRGBPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID OpenRGBPluginInterface_IID)
    Q_INTERFACES(OpenRGBPluginInterface)

public:
    OpenRGBRazerExtrasPlugin();
    ~OpenRGBRazerExtrasPlugin();

    OpenRGBPluginInfo   GetPluginInfo() override;
    unsigned int        GetPluginAPIVersion() override;

    void                Load(bool dark_theme, ResourceManager* resource_manager_ptr) override;
    QWidget*            GetWidget() override;
    QMenu*              GetTrayMenu() override;
    void                Unload() override;

    static bool             DarkTheme;
    static ResourceManager* RMPointer;

private:
    std::vector<RazerChromaAddressableRGBReset*> RazerControllers;

};

#endif // OPENRGBRAZEREXTRASPLUGIN_H
