#include "OpenRGBRazerExtrasPlugin.h"
#include <QHBoxLayout>
#include "hidapi.h"
#include "RazerDevices.h"


bool OpenRGBRazerExtrasPlugin::DarkTheme = false;
ResourceManager* OpenRGBRazerExtrasPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBRazerExtrasPlugin::GetPluginInfo()
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;

    info.Name           = "Razer extras plugin";
    info.Description    = "Plugin that allow you to control certain features on Razer devices";
    info.Version        = VERSION_STRING;
    info.Commit         = GIT_COMMIT_ID;
    info.URL            = "https://gitlab.com/OpenRGBDevelopers/OpenRGBRazerExtrasPlugin";

    info.Icon.load(":/OpenRGBRazerExtrasPlugin.png");

    info.Location       =  OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label          =  "Razer extras";
    info.TabIconString  =  "Razer extras";

    info.TabIcon.load(":/OpenRGBRazerExtrasPlugin.png");

    return info;
}

unsigned int OpenRGBRazerExtrasPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}

void OpenRGBRazerExtrasPlugin::Load(bool dark_theme, ResourceManager* resource_manager_ptr)
{
    printf("[OpenRGBRazerExtrasPlugin] Loading plugin.\n");

    RMPointer                = resource_manager_ptr;
    DarkTheme                = dark_theme;
}


QWidget* OpenRGBRazerExtrasPlugin::GetWidget()
{
    printf("[OpenRGBRazerExtrasPlugin] Creating widget.\n");

    QWidget* widget     =  new QWidget(nullptr);
    QHBoxLayout* layout = new QHBoxLayout();

    widget->setLayout(layout);

    hid_device_info*    hid_devices         = NULL;
    hid_devices = hid_enumerate(RAZER_VID, 0);

    hid_device_info*    current_hid_device;
    current_hid_device  = hid_devices;

    while(current_hid_device)
    {
        if(
                current_hid_device->vendor_id == RAZER_VID &&
                current_hid_device->product_id == RAZER_CHROMA_ADDRESSABLE_RGB_CONTROLLER_PID &&
                current_hid_device->interface_number == 0x00)
        {
            hid_device* dev = hid_open_path(current_hid_device->path);

            RazerChromaAddressableRGBReset* resetButton = new RazerChromaAddressableRGBReset(widget, dev);
            RazerControllers.push_back(resetButton);
            layout->addWidget(resetButton);
            break;
        }
        current_hid_device = current_hid_device->next;
    }

    return widget;
}

QMenu* OpenRGBRazerExtrasPlugin::GetTrayMenu()
{
    printf("[OpenRGBRazerExtrasPlugin] Creating tray menu.\n");

    QMenu* menu = new QMenu("Razer extras");

    QAction* actionReload = new QAction("Reload Razer Chroma ARGB Controller", nullptr);

    menu->addAction(actionReload);

    connect(actionReload, &QAction::triggered, [=](){
        for(RazerChromaAddressableRGBReset* RazerController:RazerControllers)
        {
            RazerController->ResetController();
        }
    });

    QAction* actionDisableRecount = new QAction("Disable Recount Chroma ARGB Controller", nullptr);

    menu->addAction(actionDisableRecount);

    connect(actionDisableRecount, &QAction::triggered, [=](){
        for(RazerChromaAddressableRGBReset* RazerController:RazerControllers)
        {
            RazerController->DisableRecount();
        }
    });

    QAction* actionEnableRecount = new QAction("Enable Recount Chroma ARGB Controller", nullptr);

    menu->addAction(actionEnableRecount);

    connect(actionEnableRecount, &QAction::triggered, [=](){
        for(RazerChromaAddressableRGBReset* RazerController:RazerControllers)
        {
            RazerController->EnableRecount();
        }
    });

    return menu;
}

void OpenRGBRazerExtrasPlugin::Unload()
{
    printf("[OpenRGBRazerExtrasPlugin] Time to call some cleaning stuff.\n");
}

OpenRGBRazerExtrasPlugin::OpenRGBRazerExtrasPlugin()
{
    printf("[OpenRGBRazerExtrasPlugin] Constructor.\n");
}

OpenRGBRazerExtrasPlugin::~OpenRGBRazerExtrasPlugin()
{
    printf("[OpenRGBRazerExtrasPlugin] Time to free some memory.\n");
}

