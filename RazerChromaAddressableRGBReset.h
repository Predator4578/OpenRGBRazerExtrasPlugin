#ifndef RAZERCHROMAADDRESSABLERGBRESET_H
#define RAZERCHROMAADDRESSABLERGBRESET_H

#include <QWidget>
#include "hidapi.h"
#include "RazerController.h"


namespace Ui {
class RazerChromaAddressableRGBReset;
}

class RazerChromaAddressableRGBReset : public QWidget
{
    Q_OBJECT

public:
    RazerChromaAddressableRGBReset(QWidget *parent, hid_device* dev);
    ~RazerChromaAddressableRGBReset();
    void ResetController();
    void DisableRecount();
    void EnableRecount();

private slots:
    void on_reset_clicked();
    void on_disable_recount_clicked();
    void on_enable_recount_clicked();

private:
    Ui::RazerChromaAddressableRGBReset *ui;
    hid_device* dev;

    razer_report razer_create_report(unsigned char command_class, unsigned char command_id, unsigned char data_size);
    unsigned char           razer_calculate_crc(razer_report* report);
    razer_report            razer_create_reset_report(unsigned char variable_storage);
    razer_report            razer_create_configure_recount_report(unsigned char recount_enabled);
    int razer_usb_send(razer_report* report);
};

#endif // RAZERCHROMAADDRESSABLERGBRESET_H
