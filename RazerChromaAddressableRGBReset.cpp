#include "RazerChromaAddressableRGBReset.h"
#include "ui_RazerChromaAddressableRGBReset.h"

RazerChromaAddressableRGBReset::RazerChromaAddressableRGBReset(QWidget *parent, hid_device* dev) :
    QWidget(parent),
    ui(new Ui::RazerChromaAddressableRGBReset),
    dev(dev)
{
    ui->setupUi(this);
}

RazerChromaAddressableRGBReset::~RazerChromaAddressableRGBReset()
{
    delete ui;
}

void RazerChromaAddressableRGBReset::ResetController()
{
    razer_report report  = razer_create_reset_report(RAZER_STORAGE_SAVE);
    razer_usb_send(&report);
}

void RazerChromaAddressableRGBReset::DisableRecount()
{
    razer_report report  = razer_create_configure_recount_report(0);
    razer_usb_send(&report);
}

void RazerChromaAddressableRGBReset::EnableRecount()
{
    razer_report report  = razer_create_configure_recount_report(1);
    razer_usb_send(&report);
}

void RazerChromaAddressableRGBReset::on_reset_clicked()
{
    ResetController();
}

void RazerChromaAddressableRGBReset::on_disable_recount_clicked()
{
    DisableRecount();
}

void RazerChromaAddressableRGBReset::on_enable_recount_clicked()
{
    EnableRecount();
}

razer_report RazerChromaAddressableRGBReset::razer_create_reset_report(unsigned char variable_storage)
{
    razer_report report         = razer_create_report(0x00, 0x36, 0x01);
    report.arguments[0]         = variable_storage;
    report.transaction_id.id = 0x1F;
    return report;
}

razer_report RazerChromaAddressableRGBReset::razer_create_configure_recount_report(unsigned char recount_enabled)
{
    razer_report report         = razer_create_report(0x00, 0x44, 0x01);
    report.arguments[0]         = recount_enabled;

    report.transaction_id.id = 0x1F;
    return report;
}

razer_report RazerChromaAddressableRGBReset::razer_create_report(unsigned char command_class, unsigned char command_id, unsigned char data_size)
{
    razer_report new_report;

    /*---------------------------------------------------------*\
    | Zero out the new report                                   |
    \*---------------------------------------------------------*/
    memset(&new_report, 0, sizeof(razer_report));

    /*---------------------------------------------------------*\
    | Fill in the new report with the given parameters          |
    \*---------------------------------------------------------*/
    new_report.report_id            = 0x00;
    new_report.status               = 0x00;
    new_report.transaction_id.id    = 0x1F;
    new_report.remaining_packets    = 0x00;
    new_report.protocol_type        = 0x00;
    new_report.command_class        = command_class;
    new_report.command_id.id        = command_id;
    new_report.data_size            = data_size;

    return new_report;
}


int RazerChromaAddressableRGBReset::razer_usb_send(razer_report* report)
{
    report->crc = razer_calculate_crc(report);

    return hid_send_feature_report(dev, (unsigned char*)report, sizeof(*report));
}

unsigned char RazerChromaAddressableRGBReset::razer_calculate_crc(razer_report* report)
{
    /*---------------------------------------------------------*\
    | The second to last byte of report is a simple checksum    |
    | Just xor all bytes up with overflow and you are done      |
    \*---------------------------------------------------------*/
    unsigned char   crc         = 0;
    unsigned char*  report_ptr  = (unsigned char*)report;

    /*---------------------------------------------------------*\
    | The start and end checks here have been modified compared |
    | to the original OpenRazer version.  This is due to adding |
    | the report ID field to the razer_report structure for     |
    | compatibility with HIDAPI.                                |
    \*---------------------------------------------------------*/
    for(unsigned int i = 3; i < 89; i++)
    {
        crc ^= report_ptr[i];
    }

    return crc;
}
